import argparse
import ipaddress
import logging
from typing import List

from lib.layer import NoFurtherContentError
from lib.layer_1 import ErfFrame
from lib.layer_3 import IPFrame

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger('filter_for_ip')
logger.setLevel(logging.INFO)


def filter_data(source: str, destination: str, ips: List[bytes]) -> None:
    """
    This method filters IP addresses as given by the arguments. In case of a match, the whole packet (including the ERF
    Frame) will be written to the destination file.

    Args:
        source: trace file to filter for the given IPs
        destination: trace file to write the matchinge packets to
        ips: list of ips in bytes the packets may match.

    Returns:
        This method returns Nothing.
    """
    with open(destination, 'wb') as outfile:
        for packet in ErfFrame.read_file(source):
            try:
                while not isinstance(packet, IPFrame):
                    packet = packet.parse_content()
            except NotImplementedError as e:
                # Currently not all possible Protocols are implemented. From time to time it may happen, that we're
                # processing a packet with yet unknown protocols. Because we're filtering for IP (for which all
                # necessary protocols are implemented), we do not need to bother with unknown protocols.
                logger.debug(e)
                continue
            except NoFurtherContentError as e:
                # The Packet has no further payload. In this case it means, the Packet does not use IPv4/IPv6
                continue

            frame_ip: IPFrame = packet

            if frame_ip.raw_source_address in ips or frame_ip.raw_destination_address in ips:
                outfile.write(packet.raw_frame)


def process_args() -> argparse.Namespace:
    """
    Method solely for parsing cli arguments.

    Returns:
        A namespace object containing the parsed arguments.
    """
    parser = argparse.ArgumentParser()

    parser.add_argument(
        '--source',
        action='store',
        dest='source',
        metavar='<PATH>',
        required=True,
        type=str
    )
    parser.add_argument(
        '--destination',
        action='store',
        default=None,
        dest='destination',
        help='destination for trace piece. Default: No output file will be created',
        metavar='<PATH>',
        required=False,
        type=str
    )
    parser.add_argument(
        '--ip',
        action='store',
        default=None,
        dest='ips',
        help='ip to filter for. May be given multiple times',
        metavar='<IP>',
        nargs='+',
        required=True
    )
    parser.add_argument(
        '-v', '--verbose',
        action='store_true',
        dest='is_verbose',
        help='Enables debug output like not yet implemented protocol types'
    )

    args = parser.parse_args()

    logger.info('source:         {}'.format(args.source))
    logger.info('destination:    {}'.format(args.destination))
    logger.info('filtered ips:   {}'.format(args.ips))
    logger.info('verbose:        {}'.format(args.is_verbose))

    return args


def main() -> None:
    args = process_args()

    if args.is_verbose:
        logger.setLevel(logging.DEBUG)
        logger.debug('verbose output activated')

    ip_list = list()
    for ip in args.ips:
        ip_list.append(ipaddress.ip_address(ip).packed)

    filter_data(
        source=args.source,
        destination=args.destination,
        ips=ip_list
    )


if __name__ == '__main__':
    main()
