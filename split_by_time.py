import argparse
import logging
import os
from typing import Iterable, List

from lib.layer_1 import ErfFrame

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger('split_by_time')


def write(source: str,
          time_interval: int,
          dest: str = None):
    """
    Writes new ERF-Files based on packet lists returned by aggregators.

    Args:
        source: trace file which will be splitted
        dest: destination directory the new pieces will be placed in. Default: Same as source
        time_interval: length of each junk in seconds

    Returns:
        Nothing
    """
    __filename = os.path.basename(source)
    if dest is None:
        destination = os.path.dirname(source)
    else:
        destination = os.path.abspath(dest)

    counter = 1
    logger.info('splitting {}'.format(source))
    for packet_list in split_by_time(source=source, time_interval=time_interval):
        source_name, source_extension = os.path.splitext(os.path.basename(source))
        destination_file = os.path.join(
            destination,
            '{filename}.{counter:0>5d}.{ext}'.format(filename=source_name, counter=counter, ext=source_extension)
        )
        counter += 1
        logger.info('{}. outputfile: {}'.format(counter, destination_file))
        with open(destination_file, 'wb') as erf_piece:
            for packet in packet_list:
                erf_piece.write(packet.raw_frame)


def split_by_time(source: str, time_interval: int) -> Iterable[List[ErfFrame]]:
    """
    This Aggregator collects packets until an interval was reached. In this case, the packets will be returned and
    a new interval starts.

    Args:
         source (str): path to the source trace
         time_interval (int): timerange in seconds one piece will span

    Returns:
        Returns lists of packets splitted by timerange.
    """
    piece_start = None
    packet_list = list()

    for packet in ErfFrame.read_file(source):
        if piece_start is None:
            piece_start = int(packet.timestamp)

        if packet.timestamp > (piece_start + time_interval):
            yield packet_list
            packet_list.clear()
            piece_start = int(packet.timestamp)

        packet_list.append(packet)

    yield packet_list


def main():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        '--source',
        action='store',
        dest='source',
        metavar='<PATH>',
        required=True,
        type=str
    )
    parser.add_argument(
        '--destination',
        action='store',
        const=None,
        dest='destination',
        help='destination directory for trace pieces. Default: same as source file',
        metavar='<PATH>',
        required=False,
        type=str
    )
    parser.add_argument(
        '--time',
        action='store',
        default=None,
        dest='interval',
        help='Time interval in seconds each trace piece contains',
        type=int
    )

    args = parser.parse_args()

    if args.destination:
        assert os.path.isdir(args.destination), 'The destination path is expected to be a directory'

    write(
        source=args.source,
        dest=args.destination,
        time_interval=args.interval
    )


if __name__ == '__main__':
    main()
