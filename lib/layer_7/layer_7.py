from lib.layer import Layer


class DfEncryptionPacket(Layer):
    """
    Implementation of the Digital Forensics Encryption packet.

    This protocol is used only internaly.

    - Byte 1-8: Content Prefix
    - Byte 9-24: internal IP the encryption Key is for (in case of IPv4, only Byte 9-12 are used)
    - Byte 25-40: external IP the encryption Key is for (in case of IPv4, only Byte 25-28 are used)
    - Byte 41-42: internal transfer port
    - Byte 43-44: external transfer port
    - Byte 45-84: the actual encryption key
    """

    def parse_content(self) -> Layer:
        pass

    @property
    def payload_offset(self) -> int:
        return len(self.raw_frame)

    @property
    def raw_prefix(self) -> bytes:
        return self._raw_frame[self._packet_offset:][0:8]

    @property
    def raw_internal_ip(self) -> bytes:
        return self._raw_frame[self._packet_offset:][8:24]

    @property
    def raw_external_ip(self) -> bytes:
        return self._raw_frame[self._packet_offset:][24:40]

    @property
    def raw_internal_port(self) -> bytes:
        return self._raw_frame[self._packet_offset:][40:42]

    @property
    def raw_external_port(self) -> bytes:
        return self._raw_frame[self._packet_offset:][42:44]

    @property
    def raw_encryption_key(self) -> bytes:
        return self._raw_frame[self._packet_offset:][44:84]
