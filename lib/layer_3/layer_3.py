import abc
import ipaddress

from lib.layer import Layer, NoFurtherContentError


class IPFrame(Layer):
    """
    This is a generic implementation for all IP headers. On its own it's not quite useful and defines only abstract
    methods.
    """
    @abc.abstractmethod
    def payload_offset(self):
        pass

    @abc.abstractmethod
    def raw_source_address(self) -> bytes:
        pass

    @abc.abstractmethod
    def raw_destination_address(self) -> bytes:
        pass


class IPv4Frame(IPFrame):
    """
    Implementation of the IPv4 packet header.

    - Byte 1 (bits 1-4): protocol version (in case of IPv4 --> '4')
    - Byte 1 (bits 5-8): header length (5 times n)
    - Byte 2: Type of service
    - Byte 3-4: total length (including payload)
    - Byte 5-6: identification
    - Byte 7 (Bits 1-7): flags
    - Byte 7-8 (Bits 8-16): fragment offset
    - Byte 9-10: protocol
    - Byte 11-12: ip header checksum
    - Byte 13-16: source ip
    - Byte 17-20: destination ip
    """
    @property
    def payload_offset(self) -> int:
        return self._packet_offset + 0

    @property
    def raw_protocol_version(self) -> bytes:
        return (self._raw_frame[self._packet_offset:][0] & 0xF0).to_bytes(length=1, byteorder='big')

    @property
    def raw_header_length(self) -> bytes:
        return (self._raw_frame[self._packet_offset:][0] & 0x0F).to_bytes(length=1, byteorder='big')

    @property
    def raw_type_of_service(self) -> bytes:
        return self._raw_frame[self._packet_offset:][1].to_bytes(length=1, byteorder='big')

    @property
    def raw_total_length(self) -> bytes:
        return self._raw_frame[self._packet_offset:][2:4]

    @property
    def raw_identification(self) -> bytes:
        return self._raw_frame[self._packet_offset:][4:6]

    @property
    def raw_flags(self) -> bytes:
        return (self._raw_frame[self._packet_offset:][6] & 0xE0).to_bytes(length=1, byteorder='big')

    @property
    def raw_fragment_offset(self) -> bytes:
        return (int(self._raw_frame[self._packet_offset:][6:8]) & 0x1FFF).to_bytes(length=2, byteorder='big')

    @property
    def raw_procotol(self) -> bytes:
        return self._raw_frame[self._packet_offset:][8:10]

    @property
    def raw_header_checksum(self) -> bytes:
        return self._raw_frame[self._packet_offset:][10:12]

    @property
    def raw_source_address(self) -> bytes:
        return self._raw_frame[self._packet_offset:][12:16]

    @property
    def raw_destination_address(self) -> bytes:
        return self._raw_frame[self._packet_offset:][16:20]

    @property
    def header_length(self) -> int:
        return int.from_bytes(self.raw_header_length, byteorder='big') * 5

    @property
    def source_address(self) -> ipaddress:
        return ipaddress.IPv4Address(self.raw_source_address)

    @property
    def destination_address(self) -> ipaddress:
        return ipaddress.IPv4Address(self.raw_destination_address)

    def parse_content(self) -> Layer:
        raise NotImplementedError()


class IPv6Frame(IPFrame):
    """
    Implementation of the IPv6 packet header.

    The IPv6 header was divided "innaturally" which means, we are not able to easily split fields by byte boundaries.

    - Byte 1 (bits 1-4): protocol version (in case of IPv6 --> '6')
    - Byte 1-2 (bits 5-12): traffic class
    - Byte 2-4 (bits 5-24): flow label
    - Byte 5-6: payload length
    - Byte 7: next header type
    - Byte 8: hop limit
    - Byte 9-24: source address
    - Byte 25-40: destination address
    """

    @property
    def payload_offset(self) -> int:
        return self._packet_offset + 0

    @property
    def raw_protocol_version(self) -> bytes:
        return (self._raw_frame[self._packet_offset:][0] & 0xF0).to_bytes(length=1, byteorder='big')

    @property
    def raw_traffic_class(self) -> bytes:
        """
        Caution: Because of the irregular positioning inside the IPv6 Header (not aligned to byte boundaries), the bits
        are shifted by 4 bits for easier access.

        Returns:

        """
        return ((self._raw_frame[self._packet_offset:][0:2] & 0x0FF0) >> 4).to_bytes(length=1, byteorder='big')

    @property
    def raw_flow_label(self) -> bytes:
        flow_label = int(self._raw_frame[self._packet_offset:][1:4])
        return (flow_label & 0x0FFFFF).to_bytes(length=3, byteorder='big')

    @property
    def raw_payload_length(self) -> bytes:
        return self._raw_frame[self._packet_offset:][4:6]

    @property
    def raw_next_header(self) -> bytes:
        return (self._raw_frame[self._packet_offset:][6]).to_bytes(length=1, byteorder='big')

    @property
    def raw_hop_limit(self) -> bytes:
        return (self._raw_frame[self._packet_offset:][7]).to_bytes(length=1, byteorder='big')

    @property
    def raw_source_address(self) -> bytes:
        return self._raw_frame[self._packet_offset:][8:24]

    @property
    def raw_destination_address(self) -> bytes:
        return self._raw_frame[self._packet_offset:][24:40]

    @property
    def source_address(self) -> ipaddress:
        return ipaddress.IPv4Address(self.raw_source_address)

    @property
    def destination_address(self) -> ipaddress:
        return ipaddress.IPv4Address(self.raw_destination_address)

    def parse_content(self) -> Layer:
        raise NotImplementedError()


class PppLcp(Layer):
    """
    Implementation of the PPP-LCP packet header.

    - Byte 1: code
    - Byte 2: identifier
    - Byte 3-4: length of packet
    - Byte 5-8: magic number
    - Byte 9-12: raw data
    """

    @property
    def raw_code(self) -> bytes:
        return self.raw_frame[self._packet_offset:][0].to_bytes(length=1, byteorder='big')

    @property
    def raw_identifier(self) -> bytes:
        return self.raw_frame[self._packet_offset:][1].to_bytes(length=1, byteorder='big')

    @property
    def raw_length(self) -> bytes:
        return self.raw_frame[self._packet_offset:][2:4]

    @property
    def raw_magic_number(self) -> bytes:
        return self.raw_frame[self._packet_offset:][4:8]

    @property
    def raw_data(self) -> bytes:
        return self.raw_frame[self._packet_offset:][8:12]

    @property
    def payload_offset(self) -> int:
        return self._packet_offset + 12

    def parse_content(self) -> Layer:
        raise NoFurtherContentError()
