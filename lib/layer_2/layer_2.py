from typing import Dict, Type, Union

from lib.layer import Layer
from lib.layer_3 import IPv4Frame, IPv6Frame
from lib.layer_3.layer_3 import PppLcp


class EthernetFrame(Layer):
    """
    Implementation of a basic ethernet packet header.

    - Byte 1-2 (bits 1-12): preamble
    - Byte 1-2 (bits 13-16):  frame delimiter
    - Byte 3-8: Source MAC address
    - Byte 9-14: Destination MAC address
    - Byte 15-16: Ethernet type
    """

    @property
    def payload_offset(self) -> int:
        return self._packet_offset + 16

    @property
    def raw_preamble(self) -> bytes:
        return self._raw_frame[self._packet_offset:][0:2] & 0xFFF0

    @property
    def raw_frame_delimiter(self) -> bytes:
        return self._raw_frame[self._packet_offset:][0:2] & 0x000F

    @property
    def raw_destination_mac(self) -> bytes:
        return self._raw_frame[self._packet_offset:][2:8]

    @property
    def raw_source_mac(self) -> bytes:
        return self._raw_frame[self._packet_offset:][8:14]

    @property
    def raw_ether_type(self) -> bytes:
        return self._raw_frame[self._packet_offset:][14:16]

    @property
    def source_mac(self) -> str:
        return self.raw_source_mac.hex()

    @property
    def destination_mac(self) -> str:
        return self.raw_destination_mac.hex()

    def parse_content(self) -> Layer:
        from lib.layer_2 import declarations

        return self._parse_content(raw_type=self.raw_ether_type, declarations=declarations.ETHER_TYPES)


class VLanFrame(Layer):
    """
    Implementation of the VLan addition for the basic ethernet packet header

    - Byte 1 (bit 1): eligible if bit is set
    - Byte 1 (bits 2-4): Priority
    - Byte 1-2 (bits 5-16): VLAN ID
    - Byte 3-4: next ethernet type (it's an addition after all)
    """
    VLAN_PRIORITIES = {
        0x00: 'Best Effort'
    }

    @property
    def raw_vlan_id(self) -> bytes:
        vlan_id = int.from_bytes(self._raw_frame[self._packet_offset:][0:2], 'big') & 0x0FFF
        return vlan_id.to_bytes(length=2, byteorder='big')

    @property
    def raw_priority(self) -> bytes:
        return (self._raw_frame[self._packet_offset:][0] & 0xE0).to_bytes(length=1, byteorder='big')

    @property
    def is_eligible(self) -> bool:
        return (self._raw_frame[self._packet_offset:][0] & 0x01) > 0

    @property
    def raw_ether_type(self) -> bytes:
        return self._raw_frame[self._packet_offset:][2:4]

    @property
    def payload_offset(self) -> int:
        return self._packet_offset + 4

    def parse_content(self) -> Layer:
        from lib.layer_2 import declarations

        return self._parse_content(raw_type=self.raw_ether_type, declarations=declarations.ETHER_TYPES)


class PppoeSessionFrame(Layer):
    """
    Implementation of the PPPoE Session addition for the basic ethernet packet header.

    - Byte 1 (Bits 1-4): PPPoE Version
    - Byte 1 (Bits 5-8): PPPoE Type
    - Byte 2: PPPoE code
    - Byte 3-4: session ID
    - Byte 5-6: length of the payload
    - Byte 7-8: the protocol inside layer 3
    """
    LAYER3_MAP: Dict[bytes, Dict[str, Union[str, Type[Layer]]]] = {
        b'\x00\x21': {
            'title': 'IPv4',
            'implementation': IPv4Frame,
        }, b'\x00\x57': {
            'title': 'IPv6',
            'implementation': IPv6Frame
        }, b'\xc0\x21': {
            'title': 'PPP Link Protocol',
            'implementation': PppLcp
        }
    }

    @property
    def payload_offset(self) -> int:
        return self._packet_offset + 8

    @property
    def raw_version(self) -> bytes:
        return (self._raw_frame[self._packet_offset:][0] & 0xF0).to_bytes(length=1, byteorder='big')

    @property
    def raw_type(self) -> bytes:
        return (self._raw_frame[self._packet_offset:][0] & 0x0F).to_bytes(length=1, byteorder='big')

    @property
    def raw_code(self) -> bytes:
        return self._raw_frame[self._packet_offset:][1].to_bytes(length=1, byteorder='big')

    @property
    def raw_session_id(self) -> bytes:
        return self._raw_frame[self._packet_offset:][2:4]

    @property
    def raw_payload_length(self) -> bytes:
        return self._raw_frame[self._packet_offset:][4:6]

    @property
    def raw_layer3_protocol(self) -> bytes:
        return self._raw_frame[self._packet_offset:][6:8]

    def parse_content(self) -> Layer:
        if self.raw_layer3_protocol in self.LAYER3_MAP and self.LAYER3_MAP[self.raw_layer3_protocol] is not None:
            return self.LAYER3_MAP[self.raw_layer3_protocol]['implementation'](
                raw_frame=self.raw_frame,
                offset=self.payload_offset
            )
        else:
            raise NotImplementedError('No implementation found for point-to-point protocol: {}'.format(
                self.raw_layer3_protocol.hex()
            ))
