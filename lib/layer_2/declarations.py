"""
This module collects central, static declarations while inside the second network layer.

Attributes:
    ETHER_TYPES (Dict[bytes, Dict[str, Union[str, Type[Layer], None]]]):
        maps the byte representation for ethernet types to the corresponding implementation if availeble. If not,
        "None" is returned. e.g.:

        | {
        |   b'\\\\x08\\\\x00': {
        |     "name": "IP Internet Protocol, Version 4 (IPv4)"
        |     "implementation": IPv4Frame
        |   },
        |   b'\\\\x08\\\\x06': {
        |     "name": "Address Resolution Protocol (ARP)",
        |     "implementation": None
        |   }
        | }

"""
from typing import Dict

from lib.layer import T_TYPE_DECLARATIONS
from lib.layer_2 import PppoeSessionFrame, VLanFrame
from lib.layer_3 import IPv4Frame, IPv6Frame

ETHER_TYPES: T_TYPE_DECLARATIONS = {
    b'\x08\x00': {
        "name": "IP Internet Protocol, Version 4 (IPv4)",
        "implementation": IPv4Frame
    },
    b'\x08\x06': {
        "name": "Address Resolution Protocol (ARP)",
        "implementation": None
    },
    b'\x08\x42': {
        "name": "Wake on LAN (WoL)",
        "implementation": None
    },
    b'\x80\x35': {
        "name": "Reverse Address Resolution Protocol (RARP)",
        "implementation": None
    },
    b'\x80\x9B': {
        "name": "AppleTalk (EtherTalk)",
        "implementation": None
    },
    b'\x80\xF3': {
        "name": "Appletalk Address Resolution Protocol (AARP)",
        "implementation": None
    },
    b'\x81\x00': {
        "name": "VLAN Tag (VLAN)",
        "implementation": VLanFrame
    },
    b'\x81\x37': {
        "name": "Novell IPX (alt)",
        "implementation": None
    },
    b'\x81\x38': {
        "name": "Novell",
        "implementation": None
    },
    b'\x86\xDD': {
        "name": "IP Internet Protocol, Version 6 (IPv6)",
        "implementation": IPv6Frame
    },
    b'\x88\x47': {
        "name": "MPLS Unicast",
        "implementation": None
    },
    b'\x88\x48': {
        "name": "MPLS Multicast",
        "implementation": None
    },
    b'\x88\x63': {
        "name": "PPPoE Discovery",
        "implementation": None
    },
    b'\x88\x64': {
        "name": "PPPoE Session",
        "implementation": PppoeSessionFrame
    },
    b'\x88\x70': {
        "name": "Jumbo Frames (veraltet)[8]",
        "implementation": None
    },
    b'\x88\x8E': {
        "name": "802.1X Port Access Entity",
        "implementation": None
    },
    b'\x88\x92': {
        "name": "Echtzeit-Ethernet PROFINET",
        "implementation": None
    },
    b'\x88\xA2': {
        "name": "ATA over Ethernet Coraid AoE[9]",
        "implementation": None
    },
    b'\x88\xA4': {
        "name": "Echtzeit-Ethernet EtherCAT",
        "implementation": None
    },
    b'\x88\xA8': {
        "name": "Provider Bridging",
        "implementation": None
    },
    b'\x88\xAB': {
        "name": "Echtzeit-Ethernet Ethernet POWERLINK",
        "implementation": None
    },
    b'\x88\xB8': {
        "name": "IEC61850 GOOSE",
        "implementation": None
    },
    b'\x88\xCC': {
        "name": "Link Layer Discovery Protocol LLDP",
        "implementation": None
    },
    b'\x88\xCD': {
        "name": "Echtzeit-Ethernet SERCOS III",
        "implementation": None
    },
    b'\x88\xE1': {
        "name": "HomePlug AV",
        "implementation": None
    },
    b'\x88\xE5': {
        "name": " MACsec",
        "implementation": None
    },
    b'\x89\x06': {
        "name": "Fibre Channel over Ethernet",
        "implementation": None
    },
    b'\x89\x14': {
        "name": "FCoE Initialization Protocol (FIP)",
        "implementation": None
    }
}
