import os
from typing import Iterator, Tuple, Union, Dict, Optional, Type

from lib.layer import Layer
from lib.layer_2 import EthernetFrame


class ErfFrame(Layer):
    """
    The Extensible Record Format was implemented using this Wireshark wiki:
        https://wiki.wireshark.org/ERF

    - Byte 0 - 3: Timestamp in seconds -> little-endian
    - Byte 4 - 7: fraction of the second -> little-endian
    - Byte 8 - 9: record length in bytes (length of packet including the erf header)
    - Byte 10: erf-flags
    - Byte 11: Type of extension header.
    - Byte 12 - 13: wire length - cite: "The exact interpretation of this quantity depends on the physical medium."
    - Byte 14 - 15: color (no precise definition found) or lost counter (packets lost while capturing because of
        overloading)
    """

    RECORD_TYPES: Dict[bytes, Dict[str, Union[str, Optional[Type[Layer]]]]] = {
        b'\x01': {
            'name': 'HDLC',
            'implementation': None
        }, b'\x02': {
            'name': 'Ethernet',
            'implementation': EthernetFrame,
        }, b'\x03': {
            'name': 'ATM',
            'implementation': None
        }, b'\x10': {
            'name': 'DSM Color Ethernet',
            'implementation': None
        }, b'\x18': {
            'name': 'Raw Link',
            'implementation': None
        }, b'\x1b': {
            'name': 'Meta',
            'implementation': None
        }, b'\x30': {
            'name': 'Pad',
            'implementation': None
        }
    }

    @staticmethod
    def read_file(file: str) -> Iterator['ErfFrame']:
        """
        Read the next packet inside the ERF-File and returns the whole frame.

        Args:
            file: string containing the path to the desired trace file.

        Returns:
            returns the next erf-frame as object of ErfFrame
        """
        assert file is not None
        assert os.path.exists(file)
        assert os.path.isfile(file)

        erf_min_header_length = 16

        with open(file, 'rb') as trace_file:
            while trace_file.readable():
                packet = trace_file.read(erf_min_header_length)

                header = ErfFrame(packet)

                if header.record_length < erf_min_header_length:
                    return None
                packet += trace_file.read(header.record_length - erf_min_header_length)
                assert header.record_length == len(packet)
                yield ErfFrame(packet)

    def __init__(self, raw_frame: bytes, offset: int = 0):
        Layer.__init__(self, raw_frame=raw_frame, offset=offset)
        self.__layers = list()

    @property
    def payload_offset(self) -> int:
        """
        The offset inside the stream to get to the next layer. In case of the ERF header, this is the length of the
        header.

        Returns:
            Returns the offset of the next layer.
        """
        return self._packet_offset + 16

    @property
    def raw_timestamp(self) -> Tuple[bytes, bytes]:
        """
        Returns the bytes containing the timestamp 1:1 like the trace does. The first tuple contains seconds (unix
        epoch time), the second contains the fraction (nth of a second).

        Returns:
            Returns a tuple containing both time parts.
        """
        return self._raw_frame[4:8], self._raw_frame[0:4]

    @property
    def timestamp(self) -> float:
        """
        Unix Epoch time (seconds since 1970-01-01 00:00:00.0)

        Returns:
            Returns the formatted timestamp, when the packet was recorded.
        """
        ts = int.from_bytes(self.raw_timestamp[0], byteorder='little')
        ts += int.from_bytes(self.raw_timestamp[1], byteorder='little') / 0xffffffff
        return ts

    @property
    def raw_record_length(self) -> bytes:
        """
        The Record length is the length of the packet including the ERF header. Therefore:
            record_length - erf_header_length = wire_length

        Returns:
            Returns the record length as 2-bytes just like inside the stream.
        """
        return self._raw_frame[10:12]

    @property
    def record_length(self) -> int:
        """
        Just like the raw_record_length, this is the size in bytes of the whole Frame including the ERF-Header.

        Returns:
            Returns the record length in bytes parsed as integer.
        """
        return int.from_bytes(self.raw_record_length, byteorder='big')

    @property
    def raw_flags(self) -> bytes:
        """
        Possible flags inside the ERF-Header.

        Returns:
            Returns flags as set inside the ERF-Header for processing.
        """
        return bytes(self._raw_frame[9:10])

    @property
    def raw_type(self) -> bytes:
        """
        Raw type of the header without further parsing.

        Returns:
            the ERF-Header type as bytes.
        """
        return bytes(self._raw_frame[8:9])

    @property
    def has_extension_header(self) -> bool:
        """
        This method checks the ERF-Header for possible extensions. Extensions are basically optional so this checks,
        whether there is an extension to parse or not.

        Returns:
            True if the extension-bit is set, false if not.
        """
        return (self.raw_type[0] & 0x40) > 0

    @property
    def raw_wire_length(self) -> bytes:
        """
        The wire length is the actualy transmitted length. ERF (and most other formats) wraps each packet inside an
        internal header. This is the packet length without these headers. The value behind this are the same on
        different formats like Pcap PcapNG etc.

        Returns:
            Returns the originally transfered bytes as bytes object.
        """
        return self._raw_frame[14:16]

    @property
    def raw_loss_counter(self) -> bytes:
        """
        This method searches the loss counter as defined inside the ERF-Header.

        Returns:
            Returns the loss-counter fields inside the ERF-Header in bytes.
        """
        return self._raw_frame[12:14]

    def parse_content(self) -> Layer:
        """
        This Method detects the sublayer and returns the corresponding instance. See :func:`~ErfFrame.RECORD_TYPES` for
        possible sublayers.

        Returns:
            An object containing the parsed sublayer.
        """

        if self.raw_type in self.RECORD_TYPES and self.RECORD_TYPES[self.raw_type]['implementation'] is not None:
            return self.RECORD_TYPES[self.raw_type]['implementation'](
                raw_frame=self.raw_frame,
                offset=self.payload_offset
            )
        else:
            raise NotImplementedError('No implementation found for record type {}'.format(self.raw_type.hex()))
