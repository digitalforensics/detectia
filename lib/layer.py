import abc
from typing import Dict, Optional, Type, Union

T_TYPE_DECLARATIONS = Dict[bytes, Dict[str, Optional[Union[str, Type["Layer"]]]]]


class Layer(abc.ABC):
    """
    A Layer is the umbrella term for packet headers. Layers are the baseclass for all protocol implementations and
    require them to implement some common methods for parsing content.
    """

    def __init__(self, raw_frame: bytes, offset: int = 0):
        self._raw_frame = raw_frame
        self._packet_offset = offset

    @property
    def raw_frame(self) -> bytes:
        return self._raw_frame

    @property
    @abc.abstractmethod
    def payload_offset(self) -> int:
        pass

    @abc.abstractmethod
    def parse_content(self) -> "Layer":
        """
        This method takes the current layer and tries to get the (parsed) payload. The implementation can be overridden
        by every protocol and should be used for external
        Returns:

        """
        pass

    def _parse_content(
        self, raw_type: bytes, declarations: T_TYPE_DECLARATIONS
    ) -> "Layer":
        """
        This method exists for code deduplication purposes. The protocol would provide the implementation Dictionary and
        the requested type. Basically the method does only search the dictionary for the key and throws a
        NotImplementedError in case no implementation could be found.

        Args:
            raw_type: the type key as row bytes.
            declarations: Dictionary mapping the raw type to the corresponding implementation dict (see
                layer2.declarations.ETHER_TYPES for an example

        Returns:
            Returns the implementation class if found

        Raises:
            Throws NotImplementedError in case no implementation could be found.
        """
        if (
            raw_type in declarations
            and declarations[raw_type]["implementation"] is not None
        ):
            ether_type_implementation = declarations[raw_type]["implementation"]
            return ether_type_implementation(
                raw_frame=self.raw_frame, offset=self.payload_offset
            )
        else:
            raise NotImplementedError(
                "No implementation found for record type {}".format(raw_type.hex())
            )


class NoFurtherContentError(ValueError):
    """
    The Protocol on the highest layer (in general layer 7, exceptions like PPP-LCP exist) has to raise this exception
    to communicate the parser to stop reading the packet.
    """

    pass
