def copy_bytes_from_file(source: str, destination: str, start: int, length: int) -> None:
    """
    This method copies the data from source to destination.

    Args:
        source: source file to copy from
        destination: destination file to copy to
        start: start offset in bytes inside the file
        length: piece length to extract in bytes.

    Returns:
        Nothing
    """
    import os

    assert os.path.getsize(source) >= (start + length)
    assert not os.path.exists(destination) or os.path.isfile(destination), \
        ValueError('The Destination has to be a file. Directories are not allowed'.format(destination))

    destination_path = os.path.abspath(destination)

    with open(source, 'rb') as infile, open(destination_path, 'wb') as outfile:
        infile.seek(start)
        outfile.write(infile.read(length))
