import argparse
import logging
import os
from typing import Iterable, List

from lib.layer_1 import ErfFrame

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger('split_by_size')


def write(source: str,
          size: int,
          dest: str = None):
    """
    Writes new ERF-Files based on packet lists returned by aggregators.

    Args:
        source: trace file which will be splitted
        dest: destination directory the new pieces will be placed in. Default: Same as source
        size: size of each junk in bytes

    Returns:
        Nothing
    """
    __filename = os.path.basename(source)
    if dest is None:
        destination = os.path.dirname(source)
    else:
        destination = os.path.abspath(dest)

    counter = 1
    logger.info('splitting {}'.format(source))
    for packet_list in split_by_size(source=source, size=size):
        source_name, source_extension = os.path.splitext(os.path.basename(source))
        destination_file = os.path.join(
            destination,
            '{filename}.{counter:0>5d}.{ext}'.format(filename=source_name, counter=counter, ext=source_extension)
        )
        counter += 1
        logger.info('{}. outputfile: {}'.format(counter, destination_file))
        with open(destination_file, 'wb') as erf_piece:
            for packet in packet_list:
                erf_piece.write(packet.raw_frame)


def split_by_size(source: str, size: int) -> Iterable[List[ErfFrame]]:
    """
    This Aggregator collects packets by their space. When reaching the desired size, those packets will be returned.

    Args:
        source (str): path to the source trace
        size (int): maximum size of each piece.

    Returns:
        Returns lists of packets with a total size up to the given limit.
    """
    packet_list = list()
    piece_size = 0

    for packet in ErfFrame.read_file(source):
        piece_size += packet.record_length

        if piece_size >= size:
            yield packet_list
            packet_list.clear()
            piece_size = 0

        packet_list.append(packet)

    yield packet_list


def main():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        '--source',
        action='store',
        dest='source',
        metavar='<PATH>',
        required=True,
        type=str
    )
    parser.add_argument(
        '--destination',
        action='store',
        const=None,
        dest='destination',
        help='destination directory for trace pieces. Default: same as source file',
        metavar='<PATH>',
        required=False,
        type=str
    )
    parser.add_argument(
        '--size',
        action='store',
        default=None,
        dest='size',
        help='maximum size in byte each trace piece contains',
        type=int
    )

    args = parser.parse_args()

    if args.destination:
        assert os.path.isdir(args.destination), 'The destination path is expected to be a directory'

    write(
        source=args.source,
        dest=args.destination,
        size=args.size
    )


if __name__ == '__main__':
    main()
