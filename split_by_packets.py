"""
This module is an example implementation for Detectia. When executing this with an ERF-File, this script will split
the file into many, smaller source files containing exactly N packets in each slice. N can be configured using the
commandline parameters.

Each slice is fully functional on its own. This means, if your trace is several Gigabytes in size, you can split them
in maybe 100.000 each. This will result in Filesizes of < 143MB per slice (assuming an maximum packet size of 1500
Bytes).

:math:`100.000 * 1.500 b = 150.000.000 b = 143,051 mb`

The basic process is to read as many files as you want inside each slice, and write them to the destination file. After
this, the cache will be dropped and we continue with the next slice. This consumes by nature quite much memory
depending on the size of each slice. One would optimize this by reading the start and end offset of each slize and then
copy the pieces with a self defined buffer (e.g. 1 MB). The memory issue would be fixed, but the complexity would
slightly increase which is not the intention for this example.
"""
import argparse
import logging
import os
from typing import Iterable, List

from lib.layer_1 import ErfFrame

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger('split_by_packets')


def write(source: str,
          count: int,
          dest: str = None):
    """
    Writes new ERF-Files based on packet lists returned by aggregators.

    Args:
        source: trace file which will be splitted
        dest: destination directory the new pieces will be placed in. Default: Same as source
        count: number of packets in each junk

    Returns:
        Nothing
    """
    __filename = os.path.basename(source)
    if dest is None:
        destination = os.path.dirname(source)
    else:
        destination = os.path.abspath(dest)

    counter = 1
    logger.info('splitting {}'.format(source))
    for packet_list in split_by_count(source=source, limit=count):
        source_name, source_extension = os.path.splitext(os.path.basename(source))
        destination_file = os.path.join(
            destination,
            '{filename}.{counter:0>5d}.{ext}'.format(filename=source_name, counter=counter, ext=source_extension)
        )
        counter += 1
        logger.info('{}. outputfile: {}'.format(counter, destination_file))
        with open(destination_file, 'wb') as erf_piece:
            for packet in packet_list:
                erf_piece.write(packet.raw_frame)


def split_by_count(source: str, limit: int) -> Iterable[List[ErfFrame]]:
    """
    This Aggregator collects packets by their number. When reaching the limit, those n packets will be returned.

    Args:
        source (str): path to the source trace
        limit (int): number of packets one piece will contain

    Returns:
        Returns lists of packets simply splittet by their number.
    """
    packet_list = list()
    packet_count = 0

    for packet in ErfFrame.read_file(source):
        if packet_count >= limit:
            yield packet_list
            packet_list.clear()
            packet_count = 0

        packet_list.append(packet)
        packet_count += 1

    yield packet_list


def __main():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        '--source',
        action='store',
        dest='source',
        metavar='<PATH>',
        required=True,
        type=str
    )
    parser.add_argument(
        '--destination',
        action='store',
        const=None,
        dest='destination',
        help='destination directory for trace pieces. Default: same as source file',
        metavar='<PATH>',
        required=False,
        type=str
    )
    parser.add_argument(
        '--count',
        action='store',
        default=None,
        dest='count',
        help='Packet count each trace piece contains',
        type=int
    )

    args = parser.parse_args()

    if args.destination:
        assert os.path.isdir(args.destination), 'The destination path is expected to be a directory'

    write(
        source=args.source,
        dest=args.destination,
        count=args.count
    )


if __name__ == '__main__':
    __main()
