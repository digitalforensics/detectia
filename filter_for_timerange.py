import argparse
import logging
import math
from typing import Tuple

import helper
from lib.layer_1 import ErfFrame

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger('filter_for_timerange')


def filter_data(source: str, lower_bound: float, upper_bound: float) -> Tuple[int, int]:
    """
    This algorithm searches the ERF-Frames for the specified timerange. Packets matching the timespan between the lower
    and the upper bounds will be written out to the destination file.

    We're asserting, that Packets inside the trace are in chronological order. So, as soon as we're detecting a packet
    with a timestamp greater then the upper limit, we cancel reading as there should not be any earlier.

    Args:
        source: source File for reading in Extensible Record Format
        lower_bound: the lower bound for the timerange in Unix Epch Time including fractional seconds.
        upper_bound: the upper bound for the timerange in Unix Epch Time including fractional seconds.

    Returns:
        returns a Tuple containing the start offset and the length for the timerange.
    """
    current_offset = 0
    start_offset = -1

    for erf_frame in ErfFrame.read_file(source):
        if lower_bound <= erf_frame.timestamp <= upper_bound:
            if start_offset < 0:
                start_offset = current_offset
        elif erf_frame.timestamp > upper_bound:
            return start_offset, current_offset - start_offset

        current_offset += erf_frame.record_length


def process_args() -> argparse.Namespace:
    """
    Method solely for parsing cli arguments.

    Returns:
        A namespace object containing the parsed arguments.
    """
    parser = argparse.ArgumentParser()

    parser.add_argument(
        '--source',
        action='store',
        dest='source',
        metavar='<PATH>',
        required=True,
        type=str
    )
    parser.add_argument(
        '--destination',
        action='store',
        default=None,
        dest='destination',
        help='destination for trace piece. Default: No output file will be created',
        metavar='<PATH>',
        required=False,
        type=str
    )
    parser.add_argument(
        '--start-ts',
        action='store',
        default=0,
        dest='start_ts',
        help='Start time in seconds (Unix Epoch Time). Only packets younger than this will pass. Fractions of seconds '
             'are allowed. Default: 0 (no lower limit)',
        metavar='<TS>',
        type=float
    )
    parser.add_argument(
        '--end-ts',
        action='store',
        default=math.pow(2, 64),
        dest='end_ts',
        help='End time in seconds (Unix Epoch Time). Only packets older than this will pass. Fractions of seconds '
             'are allowed. Default: 2^64 (literaly no upper limit)',
        metavar='<TS>',
        type=float
    )

    return parser.parse_args()


def main() -> None:
    args = process_args()

    lower_bound = args.start_ts
    upper_bound = args.end_ts

    logger.info('source:                  {}'.format(args.source))
    logger.info('destination:             {}'.format(args.destination))
    logger.info('start timestamp:         {}'.format(lower_bound))
    logger.info('end timestamp:           {}'.format(upper_bound))

    start, length = filter_data(
        source=args.source,
        lower_bound=lower_bound,
        upper_bound=upper_bound
    )

    logger.info('start offset in bytes:   {}'.format(start))
    logger.info('end offset in bytes:     {}'.format(start + length))
    logger.info('piece length in bytes:   {}'.format(length))

    if args.destination:
        helper.copy_bytes_from_file(
            source=args.source,
            destination=args.destination,
            start=start,
            length=length
        )


if __name__ == '__main__':
    main()
